# Run Development

```sh
$  docker-compose -f development.yml -p inos-frontend-development up --build -d
```

# Run production

```sh
$ docker-compose -f production.yml -p inos-frontend-production up --build -d
```

# Run bash in a Docker container (Development)

```sh
$ docker-compose -f development.yml exec inos-frontend-development sh
```

# Run bash in a Docker container (production)

```sh
$ docker-compose -f production.yml exec inos-frontend-production sh
```

# Docker

## Remove unused images

```sh
$ docker image prune
```
