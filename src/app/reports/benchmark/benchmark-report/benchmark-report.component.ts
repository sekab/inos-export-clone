import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  OnDestroy,
} from '@angular/core';
import { BenchmarkService } from '../services/benchmark.service';
import { IReportResult } from '../types/report';
import { KpisComponent } from '../../reports-components/kpis/kpis.component';
import { IExportPdfRequest } from '../types/request';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-benchmark-report',
  templateUrl: './benchmark-report.component.html',
  styleUrls: ['./benchmark-report.component.css'],
})
export class BenchmarkReportComponent implements OnInit, OnDestroy {
  reportResult: IReportResult;
  pdfExportRequest: IExportPdfRequest;
  @Output() newReportEmitter = new EventEmitter();
  @ViewChild(KpisComponent) KpiChild: KpisComponent;

  constructor(
    private router: Router,
    private benchmarkService: BenchmarkService,
  ) {}

  ngOnInit() {
    window['showMap'] = (obj: IExportPdfRequest) => {
      this.pdfExportRequest = obj;
      this.reportResult = undefined;
      setTimeout(() => {
        document.getElementById('calculateReport').click();
      }, 0);
    };
    if (!environment.production) {
      window['showMap']({
        chartTypes: {
          'LTE.ServingCells.0.RSRP': 'bar',
          'LTE.Serving.PCI': 'bar',
          'Special.Technology Penetration': 'bar',
        },
        reportDate: '22/10/2018',
        reportTitle: 'test',
        reportCity: 'test',
        reportNokiaBsc: 'test',
        reportHuwaeiBsc: 'test',
        displaySites: false,
        requestUrl: `http://localhost:3018/driveTest/benchmark/calculateResult/IYnefuBZcy`,
        token:
          // tslint:disable-next-line:max-line-length
          'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVjNTliZmI5OGQ3MjY1NzBkYWY2ZWNjYyIsInVzZXJuYW1lIjoiaW5vcyIsImVtYWlsIjoiaW5vcyIsInByaXZpbGVnZXMiOnsibGl2ZV9tb25pdG9yaW5nIjp7InB1cmNoYXNlZCI6dHJ1ZSwiZXhwaXJ5IjoiMjAzMC0wMS0zMVQyMjowMDowMC4wMDBaIn0sInBsYW5uaW5nIjp7InB1cmNoYXNlZCI6dHJ1ZSwiZXhwaXJ5IjoiMjAzMC0wMS0zMVQyMjowMDowMC4wMDBaIn0sImdlbyI6eyJwdXJjaGFzZWQiOnRydWUsImV4cGlyeSI6IjIwMzAtMDEtMzFUMjI6MDA6MDAuMDAwWiJ9LCJvcHRpbWl6YXRpb24iOnsicHVyY2hhc2VkIjp0cnVlLCJleHBpcnkiOiIyMDMwLTAxLTMxVDIyOjAwOjAwLjAwMFoifSwic29uIjp7InB1cmNoYXNlZCI6dHJ1ZSwiZXhwaXJ5IjoiMjAzMC0wMS0zMVQyMjowMDowMC4wMDBaIn0sImNvbmZpZ3VyYXRpb25fYnJvd3NlciI6eyJwdXJjaGFzZWQiOnRydWUsImV4cGlyeSI6IjIwMzAtMDEtMzFUMjI6MDA6MDAuMDAwWiJ9LCJrcGlfYnJvd3NlciI6eyJwdXJjaGFzZWQiOnRydWUsImV4cGlyeSI6IjIwMzAtMDEtMzFUMjI6MDA6MDAuMDAwWiJ9LCJjdXN0b21lcl9leHBlcmllbmNlIjp7InB1cmNoYXNlZCI6dHJ1ZSwiZXhwaXJ5IjoiMjAzMC0wMS0zMVQyMjowMDowMC4wMDBaIn0sInRyYWNlIjp7InB1cmNoYXNlZCI6dHJ1ZSwiZXhwaXJ5IjoiMjAzMC0wMS0zMVQyMjowMDowMC4wMDBaIn0sImF1dG9tYXRpY19kdCI6eyJwdXJjaGFzZWQiOnRydWUsImV4cGlyeSI6IjIwMzAtMDEtMzFUMjI6MDA6MDAuMDAwWiJ9LCJhdXRvbWF0aWNfcmVwb3J0aW5nIjp7InB1cmNoYXNlZCI6dHJ1ZSwiZXhwaXJ5IjoiMjAzMC0wMS0zMVQyMjowMDowMC4wMDBaIn0sImJlbmNobWFyayI6eyJwdXJjaGFzZWQiOnRydWUsImV4cGlyeSI6IjIwMzAtMDEtMzFUMjI6MDA6MDAuMDAwWiJ9LCJzc3YiOnsicHVyY2hhc2VkIjp0cnVlLCJleHBpcnkiOiIyMDMwLTAxLTMxVDIyOjAwOjAwLjAwMFoifSwic2NyaXB0X2J1aWxkZXIiOnsicHVyY2hhc2VkIjp0cnVlLCJleHBpcnkiOiIyMDMwLTAxLTMxVDIyOjAwOjAwLjAwMFoifSwia2l0X2NvbnRyb2xsZXIiOnsicHVyY2hhc2VkIjp0cnVlLCJleHBpcnkiOiIyMDMwLTAxLTMxVDIyOjAwOjAwLjAwMFoifSwic3N2X3N0YyI6eyJwdXJjaGFzZWQiOnRydWUsImV4cGlyeSI6IjIwMzAtMDEtMzFUMjI6MDA6MDAuMDAwWiJ9LCJsaXZlX21vbml0b3JpbmdfbGl2ZV90ZXN0Ijp7InB1cmNoYXNlZCI6dHJ1ZSwiZXhwaXJ5IjoiMjAzMC0wMS0zMVQyMjowMDowMC4wMDBaIn19LCJvcGVyYXRvciI6NjAyMDIsImlhdCI6MTU1Njc5NDIwNSwiZXhwIjoxNTU3Mzk5MDA1fQ.VNQENsxbNWnBIxePe0TV4X0jzobZ4RzfOoGt0Y2eEyg',
      });
    }
  }

  calculateReport() {
    if (environment.production) {
      this.benchmarkService
        .calculate(
          this.pdfExportRequest.requestUrl,
          this.pdfExportRequest.token,
        )
        .then(response => {
          this.reportResult = response['data'];
          setTimeout(() => {
            document.getElementById(
              'report-date',
            ).innerText = this.pdfExportRequest.reportDate;
            document.getElementById(
              'report-name',
            ).innerText = this.pdfExportRequest.reportTitle;
            document.getElementById(
              'reportTitle',
            ).innerText = this.pdfExportRequest.reportTitle;
            document.getElementById(
              'reportCity',
            ).innerText = this.pdfExportRequest.reportCity;
            document.getElementById(
              'reportNokiaBsc',
            ).innerText = this.pdfExportRequest.reportNokiaBsc;
            document.getElementById('reportNokiaBsc2').innerText = `(${
              this.pdfExportRequest.reportNokiaBsc
            })`;
            document.getElementById(
              'reportHuwaeiBsc',
            ).innerText = this.pdfExportRequest.reportHuwaeiBsc;
          }, 0);
        });
    } else {
      this.benchmarkService
        .testReport()
        .toPromise()
        .then(response => {
          this.reportResult = response['data'];
          setTimeout(() => {
            document.getElementById(
              'report-date',
            ).innerText = this.pdfExportRequest.reportDate;
            document.getElementById(
              'report-name',
            ).innerText = this.pdfExportRequest.reportTitle;
            document.getElementById(
              'reportTitle',
            ).innerText = this.pdfExportRequest.reportTitle;
            document.getElementById(
              'reportCity',
            ).innerText = this.pdfExportRequest.reportCity;
            document.getElementById(
              'reportNokiaBsc',
            ).innerText = this.pdfExportRequest.reportNokiaBsc;
            document.getElementById('reportNokiaBsc2').innerText = `(${
              this.pdfExportRequest.reportNokiaBsc
            })`;
            document.getElementById(
              'reportHuwaeiBsc',
            ).innerText = this.pdfExportRequest.reportHuwaeiBsc;
          }, 0);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  /**
   * @returns {any[]}
   * gets iteratable array
   * of events
   */
  getEventsAsArray(): any[] {
    return Object.keys(this.reportResult.events);
  }

  ngOnDestroy() {
    this.benchmarkService.reportResult = null;
  }
  /**
   * navigates back to the user-input for a new report
   */
  newReport() {
    this.router.navigate(['/modules/benchmark/user-input']);
  }
  exportPDF() {
    this.KpiChild.kpiSettings.chartOptions[0] = 'line';
  }
}
