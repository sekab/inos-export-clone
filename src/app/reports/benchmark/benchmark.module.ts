import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { BenchmarkReportComponent } from './benchmark-report/benchmark-report.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReduceTextPipe } from './services/reduce-text.pipe';
import { BenchmarkMainComponent } from './benchmark-main/benchmark-main.component';
import { ReportsComponentsModule } from '../reports-components/reports-components.module';
import { FooterComponent } from 'src/app/theme/footer/footer.component';
import { PrimeNgModule } from 'src/app/prime-ng.module';


const routes: Routes = [

  { path: '', component: BenchmarkMainComponent,
    children: [
      { path: '', redirectTo: 'report', pathMatch: 'full' },
      { path: 'report', component: BenchmarkReportComponent },
    ]
  }
];

@NgModule({
  declarations: [
    BenchmarkReportComponent,
    BenchmarkMainComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
    ReportsComponentsModule,
    PrimeNgModule,
    FormsModule,
    NgJsonEditorModule,
    RouterModule.forChild(routes),
    NgbModule,
  ],
  providers: [
    ReduceTextPipe,
  ]
})

export class BenchmarkModule { }
