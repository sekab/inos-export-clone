export interface IImsi {
    imsi: string;
    operator?: string ;
    color: string;

    samplesCount?: number;
    logFiles?: string[];
    logFilesNames?: string[];

    startTime?: Date;
    endTime?: Date;

}

export interface IImsiLog {
    name: string;
    file: string;
}
