import { IImsi } from './IMSI';

export interface IReportRequest {
  operators: IImsi[];
  kpis: string[];
  event?: string[];
}

export interface IExportPdfRequest {
  chartTypes: any;
  reportDate: string;
  reportTitle: string;
  reportCity: string;
  reportNokiaBsc: string;
  reportHuwaeiBsc: string;
  displaySites: boolean;
  requestUrl: string;
  token: string;
}
