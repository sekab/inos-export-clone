export interface IReportResult {
    globalSummary: any;
    operators: IOperator[];
    kpis: any;
    events?: any;
}



export interface IOperator {
        operator: string;
        startTime: Date;
        endTime: Date;
        imsi: string;
        samplesCount: number;
}


export interface IKpi {
    summary: any[];
    graph: IGraph;
    graphType?: any;
    maps: any[];
}

                                                                // GRAPHS
export interface IGraph {
    labels: string[];
    datasets: IDataset[];
}

export interface IDataset {
    label: string;
    data: number[];
    backgroundColor: string;
}


                                                                // MAPS

export interface IMap {
    mapOptions: IMapOptions;
    pointsOptions: IPointOptions;
    sitesOptions: ISiteOptions;
    legendsOptions: ILegendOptions;
}
export interface IPointOptions {
    size: number;
    points: IPoint[];
}
export interface ISiteOptions {
    size: number;
    sites: ISite[];
}
export interface IMapOptions {
    id: string;
    width: string;
    height: string;
}
export interface ILegendOptions {
    legends: ILegend[];
}
export interface ILegend {
    color: string;
    legend: string;
    count: string;
    percent: string;
}

export interface IPoint {
    latitude: number;
    longitude: number;
    color: string;
}

export interface ISite {
    siteColor: string;
    latitude: number;
    longitude: number;
    sectors: ISector[];
}

export interface ISector {
    azimuth: number ;
    color: string;
}
