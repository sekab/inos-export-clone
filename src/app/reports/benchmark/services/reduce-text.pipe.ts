import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'reduceText'
})
export class ReduceTextPipe implements PipeTransform {
  /**
   * @param  {string} value
   * @param  {any} args?
   * @returns string
   * if there is an arg given then it will treat the {value}
   * as a KPI name, if not it will be an IMSI name.
   */
  transform(value: string, args?: any): string {
    if (args) {
      const arr = value.split('.');
      return arr[0] + '_' + arr[arr.length - 1];
    } else {
    const res = value.substr(0, value.length - 5 )
    .split('_')[2];
    return res;
    }
  }
}
