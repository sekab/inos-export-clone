import { Injectable } from '@angular/core';
import {  IImsi } from '../types/IMSI';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IReportRequest } from '../types/request';
import { IReportResult } from '../types/report';


@Injectable({
  providedIn: 'root'
})
export class BenchmarkService {

  public reportResult: IReportResult;
  constructor(private http: HttpClient) {

   this.reportResult = null;
   }

  /**
   * used for testing purposes to return a json instead of fetchign from the backend
   */
  testReport() {
    return this.http.get('../../../../assets/tests/test.json');
  }
  calculate(requestUrl, token): Promise<any> {
    return fetch(
      requestUrl, {
      method: 'GET', // *GET, POST, PUT, DELETE, etc.
      mode: 'cors', // no-cors, cors, *same-origin
      cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
      credentials: 'same-origin', // include, *same-origin, omit
      headers: {
          'Content-Type': 'application/json',
          'Authorization': token,
      },
      redirect: 'follow', // manual, *follow, error
      referrer: 'no-referrer', // no-referrer, *client
   }).then(res => res.json());
  }
}
