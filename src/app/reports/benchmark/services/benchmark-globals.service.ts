import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BenchmarkGlobalsService {
  public _showSites: boolean;
  public showSitesSubject$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  constructor() {
    this._showSites = true;
   }
   /**
    * toggles the showsite boolean to make the maps show or not show the sites.
    */
   toggleShowSites() {
     this._showSites = !this.showSites;
     this.showSitesSubject$.next(this._showSites);
   }
   hideSites() {
     this._showSites = false;
     this.showSitesSubject$.next(this._showSites);
   }
   showSites() {
     this._showSites = true;
     this.showSitesSubject$.next(this._showSites);
   }
}
