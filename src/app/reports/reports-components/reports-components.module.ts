import { NgModule } from '@angular/core';
import { ReduceTextPipe } from '../benchmark/services/reduce-text.pipe';
import { FormsModule } from '@angular/forms';
import { NgJsonEditorModule } from 'ang-jsoneditor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { KpisComponent } from './kpis/kpis.component';
import { SummaryComponent } from './summary/summary.component';
import { ReportsHereMapComponent } from './reports-here-map/reports-here-map.component';
import { EventsComponent } from './events/events.component';
import { PrimeNgModule } from 'src/app/prime-ng.module';


@NgModule({
  declarations: [
    EventsComponent,
    KpisComponent,
    SummaryComponent,
    ReportsHereMapComponent,
    ReduceTextPipe,
  ],
  imports: [
  CommonModule,
  PrimeNgModule,
  FormsModule,
  NgJsonEditorModule,
  NgbModule,
  ],
  providers: [
    ReduceTextPipe,
  ],
  exports: [
    KpisComponent,
    SummaryComponent,
    EventsComponent,
    ReportsHereMapComponent,
    ReduceTextPipe,
    ]
})
export class ReportsComponentsModule {}
