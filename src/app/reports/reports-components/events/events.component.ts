import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  constructor() { }
  @Input() event;
  @Input() eventName;
  @Input() operators ;

  // primeng/chart Documentation https://www.primefaces.org/primeng/#/chart
  // <p-chart [options]='chartOptions'>
  public chartPlugins: any[] = [
    {
      // If User Choose One Operator Use Legend Color For Data Legend (Like Map)
      // If User Choose Many Operators Use Operator Color For Data Legend
      beforeDraw: chart => {
        const datasetsLength = chart.config.data.datasets.length;
        for (let i = 0 ; i < datasetsLength ; ++i) {
          if (chart.config.data.datasets[i].labelColor) {
            chart.legend.legendItems[i].fillStyle = chart.config.data.datasets[i].labelColor;
            chart.legend.legendItems[i].strokeStyle = chart.config.data.datasets[i].labelColor;
          }
        }
      },
    },
  ];

  // primeng/chart Documentation https://www.primefaces.org/primeng/#/chart
  // <p-chart [plugins]='chartPlugins'>
  public chartOptions: any = {
    scales: {
      xAxes: [
        {
          maxBarThickness: 100
        }
      ],
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
  };

  ngOnInit() {

  }
}
