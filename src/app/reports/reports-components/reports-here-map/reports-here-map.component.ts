import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Input,
  OnDestroy,
} from '@angular/core';
import { environment } from 'src/environments/environment';
import { IMap, IPoint } from '../../benchmark/types/report';
import { BenchmarkGlobalsService } from '../../benchmark/services/benchmark-globals.service';
declare var H: any;

@Component({
  selector: 'app-here-map',
  templateUrl: './reports-here-map.component.html',
  styleUrls: ['./reports-here-map.component.css'],
})
export class ReportsHereMapComponent implements OnInit, OnDestroy {
  private platform: any;
  map: any;
  icons;
  ui;
  sitesGroup;
  showSites: Boolean;
  @ViewChild('map') public mapElement: ElementRef;
  @Input() mapData;
  @Input() operator;
  opt: IMap;
  public constructor(private benchmarkGlobals: BenchmarkGlobalsService) {
    this.sitesGroup = new H.map.Group();
    this.icons = {};
    this.opt = {
      mapOptions: {
        id: 'map_id',
        width: '800px',
        height: '400px',
      },
      pointsOptions: {
        points: [],
        size: 7,
      },
      sitesOptions: {
        sites: [],
        size: 3,
      },
      legendsOptions: {
        legends: [],
      },
    };
  }
  ngOnDestroy() {
    window.removeEventListener('resize', () => console.log('removed RESIZE'));
  }
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit() {
    this.showSites = false;
    this.platform = new H.service.Platform({
      app_id: environment.here_maps.app_id,
      app_code: environment.here_maps.app_code,
      useHTTPS: true,
      Language: 'AR',
    });
    const optTemp = this.mapData;
    this.opt.legendsOptions.legends = optTemp.legends;
    this.opt.pointsOptions.points = optTemp.points;
    this.opt.sitesOptions.sites = optTemp.sites;

    const pixelRatio = window.devicePixelRatio || 1;
    // adding constant default layer to the map
    const defaultLayers = this.platform.createDefaultLayers({
      tileSize: pixelRatio === 1 ? 256 : 512,
      ppi: pixelRatio === 1 ? undefined : 320,
    });
    // initializing of the map
    this.map = new H.Map(
      this.mapElement.nativeElement,
      defaultLayers.normal.map,
      {
        pixelRatio: pixelRatio,
        zoom: 10,
        center: { lat: 30.0444, lng: 31.2375 },
      },
    );
    // tslint:disable-next-line:no-unused-expression
    new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
    this.ui = H.ui.UI.createDefault(this.map, defaultLayers);

    // DRAWING POINTS

    if (this.opt.legendsOptions.legends.length > 0) {
      this.drawLegend();
    }

    // limiting the map points to 8k
    if (this.opt.pointsOptions.points.length > 0) {
      this.addPointsToMap();
    }
    this._addInosTitleToMap();
    window.addEventListener('resize', () => {
      this.map.getViewPort().resize();
    });

    if (this.opt.sitesOptions.sites.length > 0) {
      this.addSitesToMap();
    }
    this.benchmarkGlobals.showSitesSubject$.subscribe(val => {
      if (val !== null) {
        this.showSites = val;
        this.toggleSites();
      }
    });
  }

  /**
   * iterates over the locations and draws them into the map
   */
  addPointsToMap() {
    // limit of max number of points to be drawn per map.
    const limit = 5000;
    const { points, size } = this.opt.pointsOptions;
    const rate =
      points.length / limit > 1 ? Math.ceil(points.length / limit) : 1;
    const pointsGroup = new H.map.Group();
    let i, len, location: IPoint;
    // iterating over the points and adding them to a group object.
    for (i = 0, len = points.length - rate; i <= len; i += rate) {
      //
      location = points[i];
      if (location.latitude && location.longitude) {
        pointsGroup.addObject(
          new H.map.Marker(
            {
              lat: location.latitude,
              lng: location.longitude,
            },
            {
              icon: this.iconColor(location.color),
            },
          ),
        );
      }
    }
    this.map.addObject(pointsGroup);
    this.map.setViewBounds(pointsGroup.getBounds());
  }
  /**
   * @param  {number} size
   * @param  {string} color
   * @returns string
   */
  _generateSvgCircle(size: number, color: string): string {
    const svgCircle = `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="${size}" width="${size}"><circle cx="${size /
      2}" cy="${size / 2}" r="${size / 2}" fill="${color}" /></svg>`;
    return svgCircle;
  }

  // adding sites to map function
  addSitesToMap() {
    const { size, sites } = this.opt.sitesOptions;
    // const mapGroup = new H.map.Group();
    for (let siteIndex = 0, len = sites.length; siteIndex < len; ++siteIndex) {
      const { latitude, longitude, siteColor, sectors } = sites[siteIndex];
      // Site And Sectors In One SVG
      const generateSvgSiteOptions = {
        siteColor,
        size,
        sectors,
      };
      const svgSite = this._generateSvgSite(generateSvgSiteOptions);

      // Add To Map
      this.sitesGroup.addObject(
        new H.map.Marker(
          {
            lat: latitude,
            lng: longitude,
          },
          {
            icon: new H.map.Icon(svgSite),
          },
        ),
      );
    }
    this.map.addObject(this.sitesGroup);
  }
  // helper function to generate svg sites
  /**
   * @param  {any} generateSvgSiteOptions
   * @returns string
   */
  _generateSvgSite(generateSvgSiteOptions: any): string {
    const { siteColor, size, sectors } = generateSvgSiteOptions;
    const generateSvgSectorOptions = {
      size,
      sectors,
    };
    const svgSectors = this._generateSvgSectors(generateSvgSectorOptions);
    switch (size) {
      case 1:
        return `<svg
                height="10"
                width="10"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
              >
                ${svgSectors}
                <circle fill='${siteColor}' cx="50%" cy="50%" r="2" />
              </svg>`;
      case 2:
        return `<svg
                height="20"
                width="20"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
              >${svgSectors}
                <circle fill='${siteColor}' cx="50%" cy="50%" r="3.5" />
              </svg>`;
      case 3:
        return `<svg
                height="30"
                width="30"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
              >${svgSectors}
                <circle fill='${siteColor}' cx="50%" cy="50%" r="4.5" />
              </svg>`;
      default:
        return `<svg
                height="40"
                width="40"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
              >${svgSectors}
                <circle fill='${siteColor}' cx="50%" cy="50%" r="6" />
              </svg>`;
    }
  }
  // helper function to generate svg sectors, called inside of generatesvgsites functions.
  /**
   * @param  {any} generateSvgSectorOptions
   * @returns string
   */
  _generateSvgSectors(generateSvgSectorOptions: any): string {
    const { sectors, size } = generateSvgSectorOptions;
    switch (size) {
      case 1:
        return sectors.map(
          sector => `<path
                  style="transform: rotate(${sector.azimuth - 180}deg);
                  transform-origin: 50% 50%;"
                  transform="rotate(0 0 0)"
                  fill="${sector.color}"
                  xmlns="http://www.w3.org/2000/svg"
                  d="M5,5.4L2.8,9.3c0,0.4,1,0.8,2.3,0.8s2.3-0.4,2.3-0.8L5,5.4z"
                />`,
        );
      case 2:
        return sectors.map(
          sector => `<path
                style="transform: rotate(${sector.azimuth - 180}deg);
                transform-origin: 50% 50%;"
                transform="rotate(0 0 0)"
                fill="${sector.color}"
                xmlns="http://www.w3.org/2000/svg"
                d="M10,10.8l-4.5,7.7c0,0.8,2,1.5,4.5,1.5s4.5-0.7,4.5-1.5L10,10.8z"
              />`,
        );
      case 3:
        return sectors.map(
          sector => `<path
                style="transform: rotate(${sector.azimuth - 180}deg);
                transform-origin: 50% 50%;"
                transform="rotate(0 0 0)"
                fill="${sector.color}"
                xmlns="http://www.w3.org/2000/svg"
                d="M15,16.2L8.3,27.8c0,1.2,3,2.3,6.8,2.3s6.8-1.1,6.8-2.3L15,16.2z"
              />`,
        );
      default:
        return sectors.map(
          sector => `<path
                  style="transform: rotate(${sector.azimuth - 180}deg);
                  transform-origin: 50% 50%;"
                  transform="rotate(0 0 0)"
                  fill="${sector.color}"
                  xmlns="http://www.w3.org/2000/svg"
                  d="M20,21.6L11,37c0,1.6,4,3,9,3s9-1.4,9-3L20,21.6z"
                />`,
        );
    }
  }

  addLegendsToMap() {
    const mapId = this.opt.mapOptions.id;
    const { legends } = this.opt.legendsOptions;

    const mapDiv = document.getElementById(mapId);
    const legendDiv = document.createElement('div');

    let html = `<ul class="legend">`;

    legends.forEach(l => {
      html += `
            <li style="border-color: ${l.color};">
            <em>${l.legend}</em>
            <span>(${l.percent})</span>
            <span>${l.count}</span>
            </li>
            `;
    });

    html += '</ul>';

    legendDiv.innerHTML = html;

    legendDiv.classList.add('HereMapParentLegend');

    mapDiv.appendChild(legendDiv);
  }
  /**
   * draws the legends in the map
   */
  drawLegend() {
    const containerNode = document.createElement('div');
    containerNode.className = 'map-legend';
    containerNode.innerHTML +=
      '<h6 class="map-legend-headline">' + 'LEGEND' + '</h6>';
    this.opt.legendsOptions.legends.forEach(el => {
      this.icons[el.color] = new H.map.Icon(
        // tslint:disable-next-line:max-line-length
        `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="8" width="8"><circle cx="4" cy="4" r="3.2" fill="${
          el.color
        }" /></svg>`,
      );
      // tslint:disable-next-line:max-line-length
      containerNode.innerHTML += `<p><div class="map-legend-color" style="background-color:${
        el.color
      };"></div>&nbsp;&nbsp;&nbsp; ${el.legend} ${el.count} (${
        el.percent
      }) </p>`;
    });
    this.map.getElement().appendChild(containerNode);
  }
  /**
   * checks if there are sites and toggles to show or not show them in the map by
   * adding or removing their reference object {sitesGroup}
   */
  toggleSites() {
    if (this.opt.sitesOptions.sites.length > 0) {
      if (this.showSites) {
        this.map.addObject(this.sitesGroup);
      } else {
        this.map.removeObject(this.sitesGroup);
      }
    }
  }
  /**
   * @param  {string} color
   * @returns {H.map.icon}
   */
  iconColor(color) {
    if (this.icons[color]) {
      return this.icons[color];
    } else {
      this.icons[color] = new H.map.Icon(
        // tslint:disable-next-line:max-line-length
        `<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="8" width="8"><circle cx="4" cy="4" r="3.2" fill="${color}" /></svg>`,
      );
      return this.icons[color];
    }
  }
  /**
   * Add 'All Rights Reserved LyticsHub & DIGIS2' To HereMap
   *
   * @return {void}
   */
  _addInosTitleToMap() {
    (window as any).document
      .querySelectorAll('span[unselectable="on"]')
      .forEach(ele => {
        ele.innerHTML = 'All Rights Reserved LyticsHub & DIGIS2';
        ele.outerHTML = ele.outerHTML; // remove all listeners from children
      });
    (window as any).document
      .querySelectorAll(
        'a[href="https://legal.here.com/en/terms/serviceterms/us"]',
      )
      .forEach(ele => {
        ele.innerHTML = '';
      });
    (window as any).document
      .querySelectorAll('svg[version="1.1"]')
      .forEach(ele => {
        ele.innerHTML = '';
      });
  }
}
