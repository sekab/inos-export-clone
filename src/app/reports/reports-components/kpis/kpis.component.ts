import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';
import { Paginator } from 'primeng/paginator';
import { Checkbox } from 'primeng/checkbox';
import { BenchmarkGlobalsService } from '../../benchmark/services/benchmark-globals.service';
import { IExportPdfRequest } from '../../benchmark/types/request';

@Component({
  selector: 'app-kpis',
  templateUrl: './kpis.component.html',
  styleUrls: ['./kpis.component.css',
  '../../benchmark/benchmark-report/benchmark-report.component.css',
  '../../benchmark/benchmark.component.scss']
})
export class KpisComponent implements OnInit, OnDestroy {
  // 4g / 3g / 2g
  public kpiSettings: {
    page: number,
    kpiNamesArray: any[],
    selectedKPI: any,
    kpiNamesDropDown: any,
    showPages: boolean,
    showSites: boolean,
    isExporting: boolean,
    chartsTypes: string[],
    chartOptions: any[],
  };
  @Input() pdfExportRequest: IExportPdfRequest;
  @Input() kpis: Object;
  @Input() operators: any[];
  @ViewChild('paginator') paginator: Paginator;
  @ViewChild('chart') chart;
  @ViewChild('showSitesCheckBox') checkBox: Checkbox;
  kpiNamesDropDown: any;

  // primeng/chart Documentation https://www.primefaces.org/primeng/#/chart
  // <p-chart [options]='chartOptions'>
  public chartPlugins: any[] = [
    {
      // If User Choose One Operator Use Legend Color For Data Legend (Like Map)
      // If User Choose Many Operators Use Operator Color For Data Legend
      beforeDraw: chart => {
        const datasetsLength = chart.config.data.datasets.length;
        for (let i = 0 ; i < datasetsLength ; ++i) {
          if (chart.config.data.datasets[i].labelColor) {
            chart.legend.legendItems[i].fillStyle = chart.config.data.datasets[i].labelColor;
            chart.legend.legendItems[i].strokeStyle = chart.config.data.datasets[i].labelColor;
          }
        }
      },
    },
  ];

  // primeng/chart Documentation https://www.primefaces.org/primeng/#/chart
  // <p-chart [plugins]='chartPlugins'>
  public chartOptions: any = {
    scales: {
      xAxes: [
        {
          maxBarThickness: 100
        }
      ],
      yAxes: [
        {
          ticks: {
            beginAtZero: true
          }
        }
      ]
    },
  };

  constructor(private benchmarkGlobals: BenchmarkGlobalsService) {
  }
  ngOnDestroy() {
  }
  ngOnInit() {
    window.scroll(0 , 0 );
    this.kpiSettings = {
      page: 0,
      kpiNamesArray: null,
      kpiNamesDropDown: null,
      selectedKPI: undefined,
      showPages: false,
      showSites: true,
      isExporting: false,
      chartsTypes: Array(5).fill('bar'),
      chartOptions: [
        { label: 'bar', value: 'bar' },
        { label: 'line', value: 'line' },
        { label: 'radar', value: 'radar' },
      ]
    };
    this.kpiSettings.kpiNamesArray = Object.keys(this.kpis);

    this.kpiSettings.selectedKPI = this.kpiSettings.kpiNamesArray[0];

    this.kpiSettings.kpiNamesDropDown = this.getKPI_Names();

    this.kpiSettings.chartsTypes = Array(this.kpiSettings.kpiNamesArray.length).fill('bar');
    this.kpiSettings.chartsTypes = Object.values(this.pdfExportRequest.chartTypes);
    this.kpiSettings.showSites = this.pdfExportRequest.displaySites;

    setTimeout(() => {
      this.checkBox.checked = this.kpiSettings.showSites;
      if (this.checkBox.checked === true ) {
        this.benchmarkGlobals.showSites();
      } else {
        this.benchmarkGlobals.hideSites();
      }
    }, 1);
  }
  /**
   * @returns {any[]}
   * maps the kpi array to a value label array
   * so it can be used in PRIMENG dropdowns or forms.
   */
  getKPI_Names(): any[] {
    const x = Object.keys(this.kpis).map(e => {
      return {
        label: e,
        value: e,
      };
    });
    return x;
  }
  /**
   * @param  {} event
  * assignss the selectedKPI when the paginator changes
   */
  paginatorChange(event) {
    const newSelectedKPI = this.kpiSettings.kpiNamesArray[event.page];
    this.kpiSettings.selectedKPI = newSelectedKPI;
  }
  /**
   * @param  {{value:string} event
   * @param  {any}} originalEvent
   * changes the paginator when the dropdown changes.
   */
  dropDownChange(event: { value: string, originalEvent: any }) {
    const index = this.kpiSettings.kpiNamesArray.indexOf(event.value);
    this.paginator.changePage(index);
  }
  /**
   * @returns {any[]}
   * returns an array of kpis.
   * returns the full array if showpages is false and returns a single element in array
   * if not.
   */
  returnKPI_Iterator(): any[] {
    return (this.kpiSettings.showPages) ? [this.kpiSettings.selectedKPI] : this.kpiSettings.kpiNamesArray;
    // return this.kpiSettings.kpiNamesArray;
  }
  /**
   */
  public showAllKPIS() {
    this.kpiSettings.showPages = false;
  }
  /**
   * calls the global benchmark service to show or not show sitess
   */
  toggleShowSites() {
    if (this.checkBox.checked === false ) {
      this.benchmarkGlobals.hideSites();
    } else {
      this.benchmarkGlobals.showSites();
    }
  }
  /**
   * @param  {} kpi
   * @returns {number}
   */
  getIndexByKPI(kpi): number {
    const index = this.kpiSettings.kpiNamesArray.indexOf(kpi);
    return index;
  }
  /**
   */
  /**
   * @returns void
   * refresh the chart
   */
  refreshChart(): void {
    this.chart.refresh();
    setTimeout(() => {
      this.chart.reinit();

    }, 0);
  }
}
