import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { IOperator } from 'src/app/reports/benchmark/types/report';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
})
export class SummaryComponent implements OnInit , OnDestroy {

  // TODO: html code needs refactoring to be understood easily.
  @Input() globalSummary: any;
  @Input() operators: IOperator[];
  constructor() { }

  ngOnInit() {
  }
  ngOnDestroy() {
  }
  /**
   * @returns {any[]}
   */
  getArray(): any[] {
    return Object.keys(this.globalSummary).filter(k => this.globalSummary[k][0] !== null );
  }
  /**
   * @param  {[]} array
   */
  getSortedRanks(array: any[]) {
    const sorted = array.slice().sort(function(a, b) {return b - a; });
    const ranks = array.slice().map(function(v) { return sorted.indexOf(v) + 1; });
    return ranks;
  }
}
