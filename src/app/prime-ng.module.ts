import { NgModule } from '@angular/core';
import { ChartModule } from 'primeng/chart';
import { MultiSelectModule } from 'primeng/multiselect';
import { DropdownModule } from 'primeng/dropdown';
import { MessageService } from 'primeng/components/common/messageservice';
import { TabViewModule } from 'primeng/tabview';
import { TreeModule } from 'primeng/tree';
import { CalendarModule } from 'primeng/calendar';
import { SpinnerModule } from 'primeng/spinner';
import { ToastModule } from 'primeng/toast';
import {SliderModule} from 'primeng/slider';
import {PaginatorModule} from 'primeng/paginator';
import {CheckboxModule} from 'primeng/checkbox';
import {DialogModule} from 'primeng/dialog';
import {PanelModule} from 'primeng/panel';


const mods = [ToastModule, ChartModule, MultiSelectModule, DropdownModule, DialogModule, PanelModule,
   TabViewModule, TreeModule, CalendarModule, SpinnerModule, SliderModule,
  PaginatorModule , CheckboxModule];
@NgModule({
  imports: [mods],
  exports: [mods],
  providers: [MessageService]
})
export class PrimeNgModule { }
