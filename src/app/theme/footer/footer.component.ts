import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  @ViewChild('footer') footer: ElementRef;
  // tslint:disable-next-line:no-input-rename
  @Input('config') configration: any = {
    color: 'white',
    bgColor: 'transparent'
  };

  constructor() { }

  ngOnInit() {
    this.setFooterStyle();
  }
  setFooterStyle() {
    this.footer.nativeElement.style.color = this.configration.color;
  }

}
