// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  base_url: 'https://api.dev.inos.lyticshub.com',
  // base_url: 'http://localhost:3000',
  here_maps: {
    app_id: 'ViH6lQYZvNxqGBHZqePP',
    app_code: 'FQSqkL2tk81-5UFHrubUhg'
  },
  MAP_ID: 'R0GcRuD9HQC7Nu6J91Ws',
  MAP_Code: 'XzqbUyvnpvHmmzPZzLpJlg'
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
